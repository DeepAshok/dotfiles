#Some alias commands
alias ll='ls -lahG'
alias home='cd ~'
alias up='cd ..'

export PS1="\[\e[0m\]\[\e[00;36m\]\u\[\e[0m\]\[\e[00;37m\] \[\e[0m\]\[\e[00;34m\]λ \[\e[0m\]"

export PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/share/dotnet:~/.dotnet/tools:/Library/Frameworks/Mono.framework/Versions/Current/Commands:/usr/local/mysql/bin:$HOME/Library/Python/3.7/bin

# Setup chruby, installed ruby with ruby-install
source /usr/local/opt/chruby/share/chruby/chruby.sh
source /usr/local/opt/chruby/share/chruby/auto.sh

export FLUTTER_HOME=$HOME/Documents/programming/flutter/flutter
export PATH=$PATH:$FLUTTER_HOME/bin

export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/bin
export JAVA_HOME=$(/usr/libexec/java_home)
export ORACLE_HOME=$HOME/lib
export PATH=$PATH:$ORACLE_HOME

# Path to the bash it configuration
export BASH_IT=$HOME/.bash_it

# Your place for hosting Git repos. I use this for private repos.
export GIT_HOSTING='git@git.domain.com'

# Set the path nginx
export NGINX_PATH='/opt/nginx'

# Change this to your console based IRC client of choice.
export IRC_CLIENT='irssi'

# Path to the bash it configuration
export BASH_IT=$HOME/.bash_it

# Don't check mail when opening terminal.
unset MAILCHECK

# Set this to false to turn off version control status checking within the prompt for all themes
export SCM_CHECK=true

#History size
export HISTCONTROL=ignoredups:erasedups  # no duplicate entries
export HISTSIZE=100000                   # big big history
export HISTFILESIZE=100000               # big big history
shopt -s histappend                      # append to history, don't overwrite it

# Save and reload the history after each command finishes
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  
# This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  
# This loads nvm bash_completion

# Git alias
alias gst='git status'
alias gcm='git commit -m'
alias gpush='git push'
alias gpull='git pull'
alias gb='git branch'
alias gco='git checkout'
alias gfetch='git fetch'
alias gdelete='git branch -d'

# Dotfiles Git configuration
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

# Jira alias
alias jiravie='open https://viegames.atlassian.net/secure/RapidBoard.jspa?rapidView=1'

# Silence zsh default shell warning
export BASH_SILENCE_DEPRECATION_WARNING=1

# Hustle alias
alias hustle='cd ~/Documents/programming/flutter/projects/hustle/hustle'

# thefuck
eval $(thefuck --alias)
